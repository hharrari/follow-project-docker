<?php

namespace App\Command;

use App\Entity\Beer;
use App\Entity\Brewery;
use App\Entity\Style;
use App\Repository\BeerRepository;
use App\Repository\BreweryRepository;
use App\Repository\StyleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use League\Csv\Reader;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:import-beer',
    description: 'Add a short description for your command',
)]
class ImportBeerCommand extends Command
{
    const BATCH_SIZE = 1000;

    public function __construct(
        private BeerRepository $beerRepository,
        private StyleRepository $styleRepository,
        private BreweryRepository $breweryRepository,
        private EntityManagerInterface $em
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Import a beer from a CSV file');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $csv = Reader::createFromPath('%kernel.project_dir%/../open-beer-database.csv', 'r');
        $csv->setHeaderOffset(0);
        $csv->setDelimiter(';');
        $records = $csv->getRecords();

        $io->progressStart(iterator_count($records));

        $batchSize = self::BATCH_SIZE;
        $counter = 0;

        $styleCache = [];
        $breweryCache = [];

        $this->em->getConnection()->beginTransaction();

        try {
            foreach ($records as $record) {
                $this->addBeer($record, $styleCache, $breweryCache);
                $counter++;
                $io->progressAdvance();

                if (($counter % $batchSize) === 0) {
                    $this->em->flush();
                    $this->em->clear();
                    $styleCache = [];
                    $breweryCache = [];
                }
            }
            $this->em->flush();
            $this->em->clear();
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            throw $e;
        }

        $io->progressFinish();
        $io->success('Importation des bières terminée !');

        return Command::SUCCESS;
    }

    private function addBeer(array $record, array &$styleCache, array &$breweryCache): ?Beer
    {
        if (empty($record['Name'])) {
            return null;
        }

        $beer = new Beer();
        $beer->setName($record['Name']);
        $beer->setAbv(floatval($record['Alcohol By Volume']));
        $beer->setIbu(intval($record['International Bitterness Units']));
        $beer->setDescription($record['Description']);
        $style = $this->returnBeerStyle($record, $styleCache);
        $brewery = $this->returnBrewery($record, $breweryCache);

        if (!$style || !$brewery) {
            return null;
        }

        $beer->setStyle($style);
        $beer->setBrewery($brewery);
        $this->em->persist($beer);

        return $beer;
    }

    private function returnBeerStyle(array $record, array &$styleCache): ?Style
    {
        if (empty($record['Style'])) {
            return null;
        }

        $styleName = $record['Style'];

        if (isset($styleCache[$styleName])) {
            return $styleCache[$styleName];
        }

        $style = $this->styleRepository->findOneBy(['name' => $styleName]);

        if ($style) {
            $styleCache[$styleName] = $style;
            return $style;
        }

        $style = new Style();
        $style->setName($styleName);
        $this->em->persist($style);
        $styleCache[$styleName] = $style;

        return $style;
    }

    private function returnBrewery(array $record, array &$breweryCache): ?Brewery
    {
        if (empty($record['Brewer'])) {
            return null;
        }

        $breweryName = $record['Brewer'];

        if (isset($breweryCache[$breweryName])) {
            return $breweryCache[$breweryName];
        }

        $brewery = $this->breweryRepository->findOneBy(['name' => $breweryName]);
        if ($brewery) {
            $breweryCache[$breweryName] = $brewery;
            return $brewery;
        }

        $brewery = new Brewery();
        $brewery->setName($record['Brewer']);
        $brewery->setAddress($record['Address']);
        $brewery->setCity($record['City']);
        $brewery->setState($record['State']);
        $brewery->setCountry($record['Country']);
        $this->em->persist($brewery);
        $breweryCache[$breweryName] = $brewery;

        return $brewery;
    }
}
