<?php

namespace App\Repository;

use App\Entity\Brewery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Brewery>
 */
class BreweryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Brewery::class);
    }


    public function findCountryRanking(): array
    {
        return $this->createQueryBuilder('b')
            ->select('b.country, COUNT(b.id) as total')
            ->groupBy('b.country')
            ->orderBy('total', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
