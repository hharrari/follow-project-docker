<?php

namespace App\Repository;

use App\Entity\Style;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Style>
 */
class StyleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Style::class);
    }

    public function findPopularBeerStyle(): array
    {
        return $this->createQueryBuilder('s')
            ->select('s.name, COUNT(b.id) as total')
            ->join('s.beers', 'b')
            ->groupBy('s.name')
            ->orderBy('total', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
