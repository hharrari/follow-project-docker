<?php
namespace App\Controller\Brewery;

use App\Repository\BreweryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
class BreweryRankingController extends AbstractController
{
	public function __construct(private BreweryRepository $breweryRepository)
	{}

	#[Route(path: 'api/breweries/country/ranking', name: '_brewery_country_ranking', methods: ['GET'])]
	public function __invoke(): JsonResponse
	{
		$breweryRankingByContry = $this->breweryRepository->findCountryRanking();
		return $this->json($breweryRankingByContry);
	}
}